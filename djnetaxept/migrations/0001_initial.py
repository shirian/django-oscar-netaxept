# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='NetaxeptCardInformation',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('transaction_id', models.CharField(max_length=32)),
                ('issuer', models.CharField(max_length=128)),
                ('issuer_country', models.CharField(max_length=16)),
                ('masked_pan', models.CharField(max_length=32)),
                ('pan_hash', models.CharField(null=True, max_length=255)),
                ('payment_method', models.CharField(null=True, max_length=16)),
                ('expiry_date', models.DateField()),
                ('issuer_id', models.CharField(max_length=16)),
                ('user', models.ForeignKey(related_name='netaxept_card_informations', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='NetaxeptPayment',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('transaction_id', models.CharField(max_length=32)),
                ('amount', models.IntegerField(null=True, blank=True)),
                ('currencycode', models.CharField(max_length=3)),
                ('description', models.CharField(max_length=255)),
                ('ordernumber', models.CharField(max_length=32)),
                ('recurring_type', models.CharField(null=True, choices=[('S', 'S'), ('R', 'R')], max_length=1)),
                ('flagged', models.BooleanField()),
                ('responsecode', models.CharField(null=True, blank=True, max_length=3)),
                ('responsesource', models.CharField(null=True, blank=True, max_length=20)),
                ('responsetext', models.CharField(null=True, blank=True, max_length=255)),
                ('user', models.ForeignKey(related_name='netaxept_payments', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='NetaxeptTransaction',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('transaction_id', models.CharField(max_length=32)),
                ('operation', models.CharField(max_length=7, choices=[('AUTH', 'AUTH'), ('SALE', 'SALE'), ('CAPTURE', 'CAPTURE'), ('CREDIT', 'CREDIT'), ('ANNUL', 'ANNUL')])),
                ('amount', models.PositiveIntegerField(null=True, blank=True)),
                ('flagged', models.BooleanField()),
                ('responsecode', models.CharField(null=True, blank=True, max_length=3)),
                ('responsesource', models.CharField(null=True, blank=True, max_length=20)),
                ('responsetext', models.CharField(null=True, blank=True, max_length=255)),
                ('payment', models.ForeignKey(to='djnetaxept.NetaxeptPayment')),
                ('user', models.ForeignKey(related_name='netaxept_transactions', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
