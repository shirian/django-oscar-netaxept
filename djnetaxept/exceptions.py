class BaseNetaxeptException(Exception):
    def __init__(self, msg=None, obj=None, inner_exn=None):
        self.msg = msg
        self.obj = obj
        self.inner_exn = inner_exn

    def __str__(self):
        return repr(self.msg)

class PaymentNotAuthorized(BaseNetaxeptException):
    msg = 'Payment not authorized'

class AmountAlreadyCaptured(BaseNetaxeptException):
    msg = 'Amount already captured, do a CREDIT'

class NoAmountCaptured(BaseNetaxeptException):
    msg = 'No amount captured nothing to CREDIT'

class ProcessException(Exception):
    pass
