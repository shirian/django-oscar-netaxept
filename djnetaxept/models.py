from django.db import models
from djnetaxept.managers import (
    NetaxeptPaymentManager,
    NetaxeptTransactionManager
)

# we want to couple payments and transactions to a user in django.
# so we need the model
from django.contrib.auth.models import User

#http://www.betalingsterminal.no/Netthandel-forside/Teknisk-veiledning/Walkthrough/Easy--and-recurring-payment/
RECURRING_CHOICES = (
    # Easy payment
    ('S', 'S'),
    # Recurring payment
    ('R', 'R'),
)

STATUS_CHOICES = (
    ('AUTHORIZED', 'AUTHORIZED'),
    ('SALE', 'SALE'),
    ('CAPTURE', 'CAPTURE'),
    ('CREDIT', 'CREDIT'),
    ('ANNUL', 'ANNUL')
)

class NetaxeptCardInformation(models.Model):
    """
    This model stores card information returned from Nets

    The PAN (personal account number) is masked, so as to not
    breach any PCI related constraints regarding this.

    Nets returns XML, but in a JSON format, the data looks
    something like this:
    ```
    "CardInformation": {
      "Issuer": "Visa",
      "IssuerCountry": "NO",
      "MaskedPAN": "492500******0004",
      "PanHash": "f2E31EUAHOqy8RKZhO9foO0gSFg=",
      "PaymentMethod": "Visa",
      "ExpiryDate": "1801",
      "IssuerId": "3"
    }
    ```
    """
    user = models.ForeignKey(
        User,
        related_name='netaxept_card_informations'
    )

    transaction_id = models.CharField(max_length=32)

    issuer = models.CharField(max_length=128)
    issuer_country = models.CharField(max_length=16)
    masked_pan = models.CharField(max_length=32)
    # pan hash is the PCI compliant hash, that relates to the PAN
    # on Nets' servers. It is not computed as a hash from the PAN
    # it is simply a random string
    pan_hash = models.CharField(max_length=255, null=True)

    payment_method = models.CharField(max_length=16, null=True)
    expiry_date = models.DateField()
    issuer_id = models.CharField(max_length=16)

class NetaxeptPayment(models.Model):
    user = models.ForeignKey(
        User,
        related_name='netaxept_payments'
    )
    transaction_id = models.CharField(max_length=32)
    amount = models.IntegerField(null=True, blank=True)
    currencycode = models.CharField(max_length=3)
    description = models.CharField(max_length=255)
    ordernumber = models.CharField(max_length=32)

    # the recurring type is coupled to a specific pan hash.
    recurring_type = models.CharField(max_length=1,
                                      null=True,
                                      choices=RECURRING_CHOICES)

    flagged = models.BooleanField()
    responsecode = models.CharField(
        max_length=3, null=True, blank=True)
    responsesource = models.CharField(
        max_length=20, null=True, blank=True)
    responsetext = models.CharField(
        max_length=255, null=True, blank=True)

    objects = NetaxeptPaymentManager()

    def auth(self):
        return NetaxeptTransaction.objects.auth_payment(self)

    def sale(self):
        return NetaxeptTransaction.objects.sale_payment(self)

    def completed(self):
        return not self.flagged

"""

class NetaxeptRecurringPayment(NetaxeptPayment):

    recurring_type = models.CharField(max_length=1, choices=RECURRING_CHOICES)
    minimum_frequency = models.PositiveIntegerField(null=True, blank=True)
    expiry_date = models.DateField(null=True, blank=True)
"""

OPERATION_CHOICES = (
    ('AUTH', 'AUTH'),
    ('SALE', 'SALE'),
    ('CAPTURE', 'CAPTURE'),
    ('CREDIT', 'CREDIT'),
    ('ANNUL', 'ANNUL')
)

class NetaxeptTransaction(models.Model):
    user = models.ForeignKey(
        User,
        related_name='netaxept_transactions'
    )

    payment = models.ForeignKey(NetaxeptPayment)
    transaction_id = models.CharField(max_length=32)
    operation = models.CharField(max_length=7, choices=OPERATION_CHOICES)
    amount = models.PositiveIntegerField(null=True, blank=True)
    flagged = models.BooleanField()
    responsecode = models.CharField(max_length=3, null=True, blank=True)
    responsesource = models.CharField(max_length=20, null=True, blank=True)
    responsetext = models.CharField(max_length=255, null=True, blank=True)

    objects = NetaxeptTransactionManager()

    def capture(self, amount):
        return NetaxeptTransaction.objects.capture_payment(self.payment, amount)

    def credit(self, amount):
        return NetaxeptTransaction.objects.credit_payment(self.payment, amount)

    def annul(self):
        return NetaxeptTransaction.objects.annul_payment(self.payment)

    def completed(self):
        return not self.flagged
